#include <Servo.h>

Servo servo;

#define SERVO 9

int pos = 0;

void setup() {
  servo.attach(SERVO);
}

void loop() {
  for (pos = 0; pos < 90; pos += 1) {
    // in steps of 1 degree
    servo.write(pos);
    delay(20);
  }
  for (pos = 90; pos > 0; pos -= 1) {
    servo.write(pos);
    delay(20);
  }
}
