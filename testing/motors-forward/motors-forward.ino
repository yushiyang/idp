/*
 * For rear drive:
 * Left motor to M1
 * Right motor to M2
 * Battery to Power+-
 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

#define LEFT 1
#define RIGHT 2
#define SPEED 255

Adafruit_DCMotor *left;
Adafruit_DCMotor *right;

void forward(int side) {
  if (side == LEFT) left->run(BACKWARD);
  else if (side == RIGHT) right->run(FORWARD);
}

void backward(int side) {
  if (side == LEFT) left->run(FORWARD);
  else if (side == RIGHT) right->run(BACKWARD);
}

void setup() {
  Adafruit_MotorShield AFMS = Adafruit_MotorShield();
  left = AFMS.getMotor(LEFT);
  right = AFMS.getMotor(RIGHT);
  AFMS.begin();
  
}


void loop() {
   left->setSpeed(SPEED);
   right->setSpeed(SPEED);
   forward(LEFT);
   forward(RIGHT);
}
