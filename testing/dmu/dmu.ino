/*
 * Red to 5V
 * Black to GND
 * Yellow to A0
 */

#define DMU A0

void setup() {
  Serial.begin(9600);
}

void loop() {
  int dmuin;

  dmuin = analogRead(DMU);

  /* 1024 at 5V */
  Serial.print(String(dmuin) + ": " + String((float) dmuin * 5 / 1024) + "V: ");
  
  if (dmuin > 512) { /* 2.5V */
    Serial.println("15-20 cm");
  } else if (dmuin > 307) { /* 1.5V */
    Serial.println("20-40 cm or <10 cm");
  } else if (dmuin > 205) { /* 1V */
    Serial.println("40-60 cm or <10 cm");
  } else if (dmuin > 102) { /* 0.5 V */
    Serial.println("60-130 cm or <10 cm");
  } else if (dmuin > 82) { /* 0.4 V */
    Serial.println("130-150? cm or <10 cm");
  } else {
    Serial.println("OoR");
  }
  
  delay(100);
}
