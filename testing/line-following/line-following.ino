#define Collector A3
#define Cathode 13
#define LED 4
int sensor_state;

void setup() {
  Serial.begin(9600);
  pinMode(Cathode, OUTPUT);
  pinMode(Collector, INPUT);
}

void loop() {

  digitalWrite(Cathode, HIGH);
  delayMicroseconds(10);
  digitalWrite(Cathode, LOW);

  sensor_state = analogRead(Collector);
  Serial.println(sensor_state);

//  Serial.println(sensor_state);
//
//  if (sensor_state < 100) {
//    Serial.println("High");
//  }
//
//  if (sensor_state > 100) {
//    Serial.println("Low");
//  }
}
