/*
  Recover from indeterminate position back to origin
  Depends on knowing which corner robot was operating at, and the general direction in which robot was pointing

  Used to recover from any state within the scan-confirm-approach cycle
*/
void recover() {
  stopMotors();
  delay(1);

  switch(corner) {
    case 1:
      recover1();
      break;
    case 2:
      recover2();
      break;
    case 3:
      recover3();
      break;
    case 4:
      recover4();
      break;
    case 5:
      recover5();
      break;
    default:
      break;
  }

  if (gohome) {
    next(GOHOME);
  } else {
    next(MOVE0TOP);
  }
}


void recover1() {
  turnRight(SCANSPEED);
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goToWall();
  goLeft(TURN90);
  followWallBackwardToLine();

  goRight(TURN90);
}


void recover2() {
  turnLeft(SCANSPEED);
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goToWall();
  goRight(TURN90);
  goForward(WALLMANOEUVRE);
  goLeft(TURN180);
  followWallBackwardToLine();

  goRight(TURN90);
}


void recover3() {
  turnRight(SCANSPEED);
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goBackwardToLine();
  goRight(TURN90);
  followLineBackwardToWall();
}


void recover4() {
  turnRight(SCANSPEED);
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goToWall();
  goLeft(TURN90);
  goForward(WALLMANOEUVRE);
  goRight(TURN180);
  goBackwardToLine();
  goLeft(TURN90);
  goForward(WALLMANOEUVRE);
  goRight(TURN180);
  followLineBackwardToWall();
}


void recover5() {
  long int remainder = angle45Remainder();
  if (remainder > 0) {
    turnRight(SCANSPEED);
    blinkyDelay(remainder);
    stopMotors();
    delay(1);
  } else {
    turnLeft(SCANSPEED);
    blinkyDelay(remainder * -1);
    stopMotors();
    delay(1);
  }

  goBackwardToLine();
  goRight(TURN90);
  followLineBackwardToWall();
}
