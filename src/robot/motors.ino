/*
  High level movement functions: move in a direction over a specified distance/angle
  a and d are to be divided by TURNSPEED and MOVESPEED respectively
*/
void goLeft(long int a) {
  turnLeft(TURNSPEED);
  blinkyDelay(a / TURNSPEED);
  stopMotors();
  delay(1);
}
void goRight(long int a) {
  turnRight(TURNSPEED);
  blinkyDelay(a / TURNSPEED);
  stopMotors();
  delay(1);
}
void goForward(long int d) {
  forward(MOVESPEED);
  blinkyDelay(d / MOVESPEED);
  stopMotors();
  delay(1);
}
void goBackward(long int d) {
  backward(MOVESPEED);
  blinkyDelay(d / MOVESPEED);
  stopMotors();
  delay(1);
}


/*
  Slightly lower speed of side to be adjusted, for follow functions
*/
void adjustLeft(int spd) {
  left->setSpeed(spd);
  leftadjusted = true;
}
void adjustRight(int spd) {
  right->setSpeed(spd);
  rightadjusted = true;
}


/*
  Start movement in a direction at speed specified by spd
  stopMotors() stops all motors
*/
void forward(int spd) {
  left->setSpeed(spd);
  right->setSpeed(spd);
  wheelForward(LEFTMOTOR);
  wheelForward(RIGHTMOTOR);
  leftadjusted = false;
  rightadjusted = false;
}
void backward(int spd) {
  left->setSpeed(spd);
  right->setSpeed(spd);
  wheelBackward(LEFTMOTOR);
  wheelBackward(RIGHTMOTOR);
  leftadjusted = false;
  rightadjusted = false;
}
void turnLeft(int spd) {
  left->setSpeed(spd);
  right->setSpeed(spd);
  wheelBackward(LEFTMOTOR);
  wheelForward(RIGHTMOTOR);
}
void turnRight(int spd) {
  left->setSpeed(spd);
  right->setSpeed(spd);
  wheelForward(LEFTMOTOR);
  wheelBackward(RIGHTMOTOR);
}
void stopMotors() {
  left->setSpeed(0);
  right->setSpeed(0);
  left->run(FORWARD);
  right->run(FORWARD);
}


/*
  Low level movement functions; do not use run() directly
  Because right side motor is mounted in reverse
*/
void wheelForward(int side) {
  if (side == LEFTMOTOR) left->run(FORWARD);
  else if (side == RIGHTMOTOR) right->run(BACKWARD);
}
void wheelBackward(int side) {
  if (side == LEFTMOTOR) left->run(BACKWARD);
  else if (side == RIGHTMOTOR) right->run(FORWARD);
}
