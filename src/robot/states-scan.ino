/*
  Scan for the mine while turning at SCANSPEED
  Turns in the appropriate direction by calling the specific function in the scanturn array
  If a mine is found, transition to confirmFind(), and thereafter to approach()
  If no mines are found after SCANDT has elapsed, transition to movePToP() to move to the next scanning corner
*/
void angularScan() {
  scanturn[corner](SCANSPEED);

  int dmuval = dmu();
  Serial.print("Scan:");
  Serial.println(dmuval);

  if (dmuval >= DMUTHRESHOLD) {
//    Serial.println("high");
    digitalWrite(LED, HIGH);
    dmuprev = dmuval;
    next(CONFIRMFIND);
  } else if (micros() >= t0 + scantime[corner]) {
//    Serial.println("time");
    digitalWrite(LED, LOW);
    next(MOVEPTOP);
  } else {
//    Serial.println("low");
    digitalWrite(LED, LOW);
  }

  // DMU updates only once every 38300 microseconds
  while (micros() < tl + 38300) delayMicroseconds(1);
  tl = micros();
}

void preAngularScan() {
  for (int i = 0; i < DMUARRAY; i++) {
    dmu();
    while (micros() < tl + 38300) delayMicroseconds(1);
    tl = micros();
  }
  dmuvalp = 0;
  dmuvalt = 0;
  dmuprev = 0;
}


/*
  Pause to discern if mine detected is genuine
  Continue once DMUCONFIRMARRAY number of DMU values have been collected

  dmuconfirmc is the number of values in the dmuconfirms array that exceed DMUTHRESHOLD
  If dmuconfirmc meets the threshold set by DMUCONFIRMCOUNT, transition to approach()
  If not, ignore the find and transition back to scan() to continue scanning
*/
void confirmFind() {
  stopMotors();
  int dmuval = dmu();
  // Disabled because we do not loop back and overwrite
  // if (dmuconfirms[dmuconfirmp]) dmuconfirmc--;
  Serial.print("Confirm:");
  Serial.println(dmuval);
  if (dmuval > DMUTHRESHOLD) {
    dmuconfirms[dmuconfirmp] = true;
    dmuconfirmc++;
  } else {
    dmuconfirms[dmuconfirmp] = false;
  }
  dmuconfirmp++;

  // Stop upon completion of DMUCONFIRMARRAY number of readings
  if (dmuconfirmp > DMUCONFIRMARRAY) {
    if (dmuconfirmc >= DMUCONFIRMCOUNT) {
      Serial.println("hi");
      next(APPROACH);
    } else {
      Serial.println("bye");
      previous();
    }
  }

  while (micros() < tl + 38300) delayMicroseconds(1);
  tl = micros();
}
void preConfirmFind() {
  // No need to clear dmuconfirms because we overwrite it all anyway
  // Keep values in dmuvals and keep writing to it
  dmuconfirmp = 0;
  dmuconfirmc = 0;
}
