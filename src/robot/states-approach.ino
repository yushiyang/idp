/*
  Approach the mine by moving straight towards it at APPROACHSPEED, while checking that DMU values are increasing
  If DMU value decreases instead, or if DMU value falls below DMUTHRESHOLD, transition to approachScan() to attempt to relocate the mine
  If DMU value peaks by exceeding DMUFINALTHRESHOLD, or if APPROACHTIMEOUT occurs, transition to finalApproach()
*/
void approach() {
  forward(APPROACHSPEED);
  int dmuval = dmu();

  dmuapproacht = dmuapproacht - dmuapproachs[dmuapproachp] + dmuval;
  dmuapproachs[dmuapproachp] = dmuval;
  dmuapproachp++;
  if (dmuapproachp >= DMUAPPROACHARRAY) dmuapproachp = 0;

  // Increase dmuapproachn until dmuapproachs is fully populated
  if (dmuapproachn < DMUAPPROACHARRAY) dmuapproachn++;

  dmuval = dmuapproacht / dmuapproachn;
  Serial.print("Approach:");
  Serial.print(dmuprev);
  Serial.print(",");
  Serial.println(dmuval);

  // dmuval should be increasing (distance should be decreasing)
  if (dmuval + DMUTOLERANCE < dmuprev || dmuval < DMUTHRESHOLD) {
    Serial.println("approach -> approachScan");
    next(APPROACHSCAN);
    // dmuprev is untouched in approachScan
  } else if (dmuval >= DMUFINALTHRESHOLD || micros() > t0 + APPROACHTIMEOUT) {
    Serial.println("approach -> finalApproach");
    next(FINALAPPROACH);
  } else {
    dmuprev = dmuval;
  }

}
void preApproach() {
  dmuapproacht = 0;
  // Initialise dmuapproachs with values from dmuvals to reduce variance at the beginning
  for (int i = 0; i < DMUARRAY && i < DMUAPPROACHARRAY; i++) {
    dmuapproachs[i] = dmuvals[dmuvalp];
    dmuvalp++;
    dmuapproacht += dmuapproachs[i];
    if (dmuvalp >= DMUARRAY) dmuvalp = 0;
  }
  // Set the rest of dmuapproachs to 0
  for (int i = DMUARRAY; i < DMUAPPROACHARRAY; i++) {
    dmuapproachs[i] = 0;
  }
  dmuapproachp = DMUARRAY;
  if (dmuapproachp >= DMUAPPROACHARRAY) dmuapproachp = 0;
  dmuapproachn = DMUARRAY;
  if (dmuapproachn > DMUAPPROACHARRAY) dmuapproachn = DMUAPPROACHARRAY;

  // Subtract DMURESTARTTOLERANCE to mitigate the effects of noise at the beginning of approach state
  dmuprev = (dmuapproacht / dmuapproachn) - DMURESTARTTOLERANCE;
}


/*
  Attempt to relocate the mine by performing limited angular scans at APPROACHSCANSPEED, for APPROACHSCANSWEEPS number of sweeps
  If mine is found, transition to confirmFind(), and thereafter back to approach()
  If mine is not found after APPROACHSCANSWEEPS number of sweeps, transition to recover() to return to origin
*/
void approachScan() {
  if (approachscanit % 2) scanturn[corner](APPROACHSCANSPEED);
  else scanreverse[corner](APPROACHSCANSPEED);

  int dmuval = dmu();

  if (dmuval + DMUTOLERANCE >= dmuprev) {
    // Mine found
    digitalWrite(LED, HIGH);
    dmuprev = dmuval;
    Serial.println(dmuprev);
    next(CONFIRMFIND);
  } else {
    digitalWrite(LED, LOW);

    if (approachscanit == 0) {
      // Initial half sweep
      if (micros() >= t0 + APPROACHSCANTI) {
        approachscanit++;
        // Scan will reverse direction after approachscanit is incremented
      }
    } else {
      if (micros() >= t0 + APPROACHSCANTI + APPROACHSCANDT * approachscanit) {
        approachscanit++;
        // Scan will reverse direction after approachscanit is incremented
      }
    }

    if (approachscanit > APPROACHSCANSWEEPS) {
      Serial.println("approachScan -> recover");
      next(RECOVER);
    }
  }
}
// Keep values in dmuvals and keep writing to it


/*
  Final approach, including live-mine detection, in order to move mine into grabber ranger
  Thereafter, indicator LEDs are lit for 5 seconds to indicate live or dummy status, and grabber closes on the mine
  Then transition to retreat() state

  At this stage, we are sure that the mine exists within a range
  If no live mine is detected by the hall effect sensor, it must be a dummy
*/
void finalApproach() {
  digitalWrite(AMBERLED, HIGH);

  unsigned long int start = millis();
  unsigned long int t = millis();
  while (t < start + FINALDT) {
    forward(FINALSPEED);

    if (!scary && hall()) {
      Serial.println("scary");
      scary = true;

      // Ignore the remainder of FINALDT and move for FINALLIVEDT more instead; push start back by the difference
      start = t + FINALLIVEDT - FINALDT;
    }

    t = millis();
  }

  stopMotors();

  // Scary is false by default
  if (scary) {
    digitalWrite(REDLED, HIGH);
  } else {
    digitalWrite(GREENLED, HIGH);
  }

  // Wait 5 seconds
  delay(5000);

  closeGrabber();

  Serial.println("finalApproach -> retreat");
  next(RETREAT);
}
