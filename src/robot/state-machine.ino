/*
  Transition to next state by first calling the state-specific setup function, then updating global timing variables
*/
void next(int st) {
  prevstate = state;
  state = st;
  setupstate[state]();

  tp = t0;
  t0 = micros();
  if (prevstate == ANGULARSCAN) {
    tai = tp;
    taf = t0;
  }
}

/*
  Transition back to previous state by first calling the state-specific setup function, then updating global time variables
*/
void previous() {
  state = prevstate;
  prevstate = RECOVER;
  setupstate[state]();

  // Excise the time elapsed for the state we are about to leave,
  // and move the t0 of the previous state (that we are returning to) forward by that amount
  t0 = tp + (micros() - t0);
  tp = 0;
}


/*
  Transition to anchor state by first calling the state-specific setup function, then updating global time variables
*/
void anchor() {
  state = ANGULARSCAN;
  prevstate = RECOVER;
  setupstate[state]();

  // Excise the time elapsed since the end of the last anchor state,
  // and move the t0 of the anchor state (that we are returning to) forward by that amount
  t0 = tai + (micros() - taf);
  tp = 0;
}


/*
  Empty function for states that do not need setup
*/
void nop() { }


/*
  Empty function for corner 0 of scanturn and scanreverse, that does not require a turning function
*/
void nopInt(int a) { }


/*
  Initial state
  Transition to the state specified by FIRSTSTATE when button is pressed (usually MOVESTARTTO0)
*/
void waitToStart() {
  int btnin = digitalRead(BUTTON);
  if (btnin == LOW) {
    ttt = millis();
    next(FIRSTSTATE);

    openGrabber();
  }
}
