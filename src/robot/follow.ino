void goToWall() {
  while (rearwall() > WALLREARTHRESHOLD) {
    blinky();
    backward(MOVESPEED);
  }
  stopMotors();
  delay(1);
}


void goForwardToLine() {
  clearlines();

  while (!leftline() || !rightline()) {
    blinky();
    forward(FOLLOWSPEED);
  }
  stopMotors();
  delay(1);
}


void goBackwardToLine() {
  clearlines();

  while (!leftline() || !rightline()) {
    blinky();
    backward(FOLLOWSPEED);
  }
  stopMotors();
  delay(1);
}


void followWallBackwardToLine() {
  clearlines();

  backward(FOLLOWSPEED);

  while (!leftline() || !rightline()) {
    blinky();

    int d = sidewall();

    // End adjustment if back in range
    if (leftadjusted) {
      if (d <= WALLTHRESHOLD && d != -1) {
        backward(FOLLOWSPEED);
      }
    } else if (rightadjusted) {
      if (d >= WALLTHRESHOLD) {
        backward(FOLLOWSPEED);
      }
    }

    if (d > WALLTHRESHOLD + WALLTOLERANCE) {
      adjustLeft(ADJUSTSPEED);
      wheelBackward(LEFTMOTOR);
    } else if (d < WALLTHRESHOLD - WALLTOLERANCE && d != -1) {
      adjustRight(ADJUSTSPEED);
      wheelBackward(RIGHTMOTOR);
    }
  }

  stopMotors();
  delay(1);
}


void followWallBackwardToWall() {
  backward(FOLLOWSPEED);

  while (rearwall() > WALLREARTHRESHOLD) {
    blinky();

    // End adjustment if back in range
    int d = sidewall();
    if (leftadjusted) {
      if (d <= WALLTHRESHOLD && d != -1) {
        backward(FOLLOWSPEED);
      }
    } else if (rightadjusted) {
      if (d >= WALLTHRESHOLD) {
        backward(FOLLOWSPEED);
      }
    }

    if (d > WALLTHRESHOLD + WALLTOLERANCE) {
      adjustLeft(ADJUSTSPEED);
    } else if (d < WALLTHRESHOLD - WALLTOLERANCE && d != -1) {
      adjustRight(ADJUSTSPEED);
    }
  }

  stopMotors();
  delay(1);
}

// This assumes that we are already on the line
void followLineBackwardToWall() {
  backward(FOLLOWSPEED);

  while (rearwall() > WALLREARTHRESHOLD) {
    blinky();

    bool lefton = leftline();
    bool righton = rightline();

    // End adjustment if back on line
    if (leftadjusted) {
      if (righton) {
        backward(FOLLOWSPEED);
      }
    } else if (rightadjusted) {
      if (lefton) {
        backward(FOLLOWSPEED);
      }
    }

    if (!lefton) {
      adjustRight(ADJUSTSPEED);
    } else if (!righton) {
      adjustLeft(ADJUSTSPEED);
    }
  }

  stopMotors();
  delay(1);
}
