/*
  Reads DMU and returns a dimensionless value
  Maintains a sliding window of the previous DMUARRAY number of DMU readings, and returns the median value
*/
int dmu() {
  int dmuin = analogRead(DMU);

  dmuvalt = dmuvalt - dmuvals[dmuvalp] + dmuin;
  dmuvals[dmuvalp] = dmuin;
  dmuvalp++;
  if (dmuvalp >= DMUARRAY) dmuvalp = 0;

  for (int i = 0; i < DMUARRAY; i++) dmusorted[i] = dmuvals[i];
  sort(dmusorted, 0, DMUARRAY);

  return dmusorted[DMUARRAY / 2];
}


/*
  Returns rear ultrasonic sensor reading, the distance to wall, in millimetres
*/
int rearwall() {
  long echoin;

  digitalWrite(FRONTTRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(FRONTTRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(FRONTTRIGGER, LOW);

  echoin = pulseIn(FRONTECHO, HIGH, 40000);

  /* 58.1 ~= 1/0.0172; Speed of sound ~= 340 m/s */
  if (echoin <= 0) return -1;
  return (int) ((float) echoin / 5.81);
}


/*
  Returns side ultrasonic sensor reading, the distance to wall, in millimetres
*/
int sidewall() {
  long echoin;

  digitalWrite(SIDETRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(SIDETRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(SIDETRIGGER, LOW);

  echoin = pulseIn(SIDEECHO, HIGH, 40000);

  /* 58.1 ~= 1/0.0172; Speed of sound ~= 340 m/s */
  if (echoin <= 0) return -1;
  return (int) ((float) echoin / 5.81);
}


/*
  Clear line follower arrays
  Use this to prevent the previous line-following attempt from affecting the start of the next attempt
*/
void clearlines() {
  for (int i = 0; i < LINEFOLLOWARRAY; i++) {
    leftlines[i] = 1024;
    leftlinesorted[i] = 1024;

    rightlines[i] = 1024;
    rightlinesorted[i] = 1024;
  }

  leftlinep = 0;
  rightlinep = 0;
}


/*
  Returns true if on a line, and false otherwise
  Maintains a sliding window of the previous LINEFOLLOWARRAY number of line follower readings, and checks the median value against the calibrated threshold
*/
bool leftline() {
  long colin;

  digitalWrite(LEFTCATHODE, LOW);
  delayMicroseconds(2);
  digitalWrite(LEFTCATHODE, HIGH);
  delayMicroseconds(10);
  digitalWrite(LEFTCATHODE, LOW);

  colin = analogRead(LEFTCOLLECTOR);

  leftlines[leftlinep] = colin;
  leftlinep++;
  if (leftlinep >= LINEFOLLOWARRAY) leftlinep = 0;

  for (int i = 0; i < LINEFOLLOWARRAY; i++) leftlinesorted[i] = leftlines[i];
  sort(leftlinesorted, 0, LINEFOLLOWARRAY);

  long colval = leftlinesorted[LINEFOLLOWARRAY / 2];

  Serial.print("L:");
  Serial.println(colval);
  if (colval < LINELEFTTHRESHOLD) return true;
  else return false;
}


/*
  Returns true if on a line, and false otherwise
  Maintains a sliding window of the previous LINEFOLLOWARRAY number of line follower readings, and checks the median value against the calibrated threshold
*/
bool rightline() {
  long colin;

  digitalWrite(RIGHTCATHODE, LOW);
  delayMicroseconds(2);
  digitalWrite(RIGHTCATHODE, HIGH);
  delayMicroseconds(10);
  digitalWrite(RIGHTCATHODE, LOW);

  colin = analogRead(RIGHTCOLLECTOR);

  rightlines[rightlinep] = colin;
  rightlinep++;
  if (rightlinep >= LINEFOLLOWARRAY) rightlinep = 0;

  for (int i = 0; i < LINEFOLLOWARRAY; i++) rightlinesorted[i] = rightlines[i];
  sort(rightlinesorted, 0, LINEFOLLOWARRAY);

  long colval = rightlinesorted[LINEFOLLOWARRAY / 2];

  Serial.print("R:");
  Serial.println(colval);
  if (colval < LINERIGHTTHRESHOLD) return true;
  else return false;
}


/*
  Returns true if magnet detected, and false otherwise
  Uses the final output of the hall effect sensor circuit
  Maintains a sliding window of the previous HALLARRAY number of readings, and returns true when the positive count exceeds HALLCONFIRMCOUNT
*/
bool hall() {
  int hallin = digitalRead(HALL);

  if (hallvals[hallvalp]) hallvalc--;
  if (hallin == HIGH) {
    hallvals[hallvalp] = true;
    hallvalc++;
  } else {
    hallvals[hallvalp] = false;
  }

  hallvalp++;
  if (hallvalp >= HALLARRAY) hallvalp = 0;

  if (hallvalc >= HALLCONFIRMCOUNT) return true;
  else return false;
}
