/*
  Quicksort
*/
void sort(int* a, int low, int high) {
  if (low < high) {
    int pivotpoint = (low + high) / 2;
    int pivot = a[pivotpoint];
    int p = low;
    for (int i = low; i < high; i++) {
      if (a[i] < pivot) {
        int t = a[i];
        a[i] = a[p];
        a[p] = t;
        p++;
      }
    }
    if (p < pivotpoint) {
      a[pivotpoint] = a[p];
      a[p] = pivot;
    }

    sort(a, low, p);
    sort(a, p + 1, high);
  }
}


/*
  Blink amber LED (and red LED if scary) at 0.5Hz
*/
void blinky() {
  if ((millis() / 1000) % 2) {
    digitalWrite(AMBERLED, HIGH);
    if (scary) digitalWrite(REDLED, HIGH);
  } else {
    digitalWrite(AMBERLED, LOW);
    if (scary) digitalWrite(REDLED, LOW);
  }
}


/*
  Delay for dt, and bink amber LED (and red LED if scary) at 0.5Hz
*/
void blinkyDelay(unsigned long int dt) {
  unsigned long int start = millis();
  unsigned long t = millis();
  while (t < start + dt) {
    if ((t / 1000) % 2) {
      digitalWrite(AMBERLED, HIGH);
      if (scary) digitalWrite(REDLED, HIGH);
    } else {
      digitalWrite(AMBERLED, LOW);
      if (scary) digitalWrite(REDLED, LOW);
    }
    t = millis();
  }
}


/*
  Returns the remaining amount of time to rotate before proceeding with retreat
  Positive values indicate an elapsed angle of between 0 and 90 degrees
  Negative valeus indicate an elapsed angle of between 90 and 180 degrees
*/
long int angleRemainder() {
  // Elapsed time (us) = taf - tai
  // Total expected scan (us) = SCANDT / SCANSPEED
  // Remainder time (ms) = (SCANDT / SCANSPEED) - (taf - tai) / 1000
  return (SCANDT - ((taf - tai) * SCANSPEED)) / POSTSCANSPEED / 1000;
}


/*
  Returns the remaining amount of time to rotate before proceeding with retreat
  Positive values indicate an elapsed angle of between 0 and 45 degrees
  Negative valeus indicate an elapsed angle of between 45 and 90 degrees
*/
long int angle45Remainder() {
  return ((SCANDT / 2) - ((taf - tai) * SCANSPEED)) / POSTSCANSPEED / 1000;
}
