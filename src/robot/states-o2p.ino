/*
  Move from start position to origin
  Then transition to move0ToP()
*/
void moveStartTo0() {
  goForwardToLine();
  goRight(TURN90);
  followLineBackwardToWall();

  next(MOVE0TOP);
}


/*
  When time runs out, go back to starting position
  This routine must be called when the robot is already at origin position, or must be called right after recover() (which moves the robot back to origin position)
*/
void goHome() {
  // Approximate values
  goForward(MOVE0TOTOP + MOVETOPTOMID + MOVEMIDTOBOTTOM);
  goRight(TURN90);
  goForward(MOVE0TOTOP);

  next(WAITTOSTART);
}


/*
  Move from origin to current scanning corner, then transition to angularScan()
*/
void move0ToP() {
  stopMotors();
  delay(1);

  switch(corner) {
    case 1:
      move0To1();
      break;
    case 2:
      move0To2();
      break;
    case 3:
      move0To3();
      break;
    case 4:
      move0To4();
      break;
    case 5:
      move0To5();
      break;
    default:
      PANIC();
  }

  next(ANGULARSCAN);
}


void move0To1() {
  goForward(MOVE0TOTOP);
  goLeft(TURN90);
  goForward(MOVE0TOLEFT);
}


void move0To2() {
  goRight(TURN90);
  goToWall();
  goLeft(TURN90);
  goForward(MOVE0TOTOP);
  goRight(TURN90);
  goForward(MOVE0TOLEFT);
}


void move0To3() {
  goForward(WALLMANOEUVRE);
  goLeft(TURN180);
  goToWall();
  goForward(MOVE0TOTOP);
  goRight(TURN90);
  goForward(MOVE0TOLEFT);
  goLeft(TURN90);
}


void move0To4() {
  goForward(WALLMANOEUVRE);
  goLeft(TURN180);
  goToWall();
  goLeft(TURN90);
  goToWall();
  goRight(TURN90);
  goForward(MOVE0TOTOP);
  goLeft(TURN90);
  goForward(MOVE0TOLEFT);
}


void move0To5() {
  goForward(MOVE0TOTOP + MOVETOPTOMID);
  goLeft(TURN90);
  goForward(MOVE0TOLEFT + MOVELEFTTOMID);
  goLeft(TURN45);
}
