/*
  Deliver live mine from origin, then return to origin
  Transition to move0ToP() thereafter
*/
void deliverLive() {
  goRight(TURN90);
  goForward(MOVELIVE);
  openGrabber();

  // Reset scary
  scary = false;

  goBackward(MOVELIVE);
  goLeft(TURN180);
  goBackwardToLine();
  goRight(TURN90);

  next(MOVE0TOP);
}


/*
  Deliver dummy mine from origin, then return to origin
  Transition to move0ToP() thereafter
*/
void deliverDummy() {
  goForward(MOVEDUMMYLATERAL);
  goRight(TURN90);
  goForward(MOVEDUMMY);
  openGrabber();

  // Reset scary
  scary = false;

  goBackward(MOVEDUMMY);
  goLeft(TURN180);
  goBackwardToLine();
  goRight(TURN90);
  followLineBackwardToWall();

  next(MOVE0TOP);
}


/*
  Retreat back to origin after locating and grabbing a mine, then transition to deliverLive() or deliverDummy() depending on whether the mine was detected to be live or dummy
  Uses similar movement logic as the recover() routines
*/
void retreat() {
  switch (corner) {
    case 1:
    case 2:
      retreatNorth();
      break;
    case 3:
    case 5:
      retreatWest();
      break;
    case 4:
      retreatSouth();
      break;
    default:
      PANIC();
  }

  if (scary) next(DELIVERLIVE);
  else next(DELIVERDUMMY);
}


void retreatNorth() {
  // case 1 and 2 only
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  switch (corner) {
    case 1:
      turnRight(POSTSCANSPEED);
      break;
    case 2:
      turnLeft(POSTSCANSPEED);
      break;
  }
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goToWall();
  goLeft(TURN90);

  followWallBackwardToLine();

  goRight(TURN90);
}


void retreatSouth() {
  // case 4 only
  long int remainder = angleRemainder();
  if (remainder < 0) remainder = 0;
  turnRight(POSTSCANSPEED);
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goToWall();
  goRight(TURN90); // will not be within grabber range of right wall after picking up mine, no need to WALLMANOEUVRE

  goBackwardToLine();

  goLeft(TURN90);
  goForward(WALLMANOEUVRE);
  goRight(TURN180);
  followLineBackwardToWall();
}


void retreatWest() {
  // case 3 and 5 only, same direction
  long int remainder;
  if (corner == 3) {
    remainder = angleRemainder();
    if (remainder < 0) remainder = 0;
  } else {
    remainder = angle45Remainder();
  }

  if (remainder > 0) {
    turnRight(POSTSCANSPEED);
  } else if (remainder < 0) {
    turnLeft(POSTSCANSPEED);
    remainder = remainder * -1;
  }
  blinkyDelay(remainder);
  stopMotors();
  delay(1);

  goBackwardToLine();
  goRight(TURN90);

  followLineBackwardToWall();
}
