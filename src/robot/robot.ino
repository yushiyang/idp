/*
 * Front drive:
 * Left motor to M1
 * Right motor to M2
 * Battery to Power+-
 *
 * Servo:
 * to Servo 2 (uses pin 9)
 * Brown on USB side
 * Orange on digital pin side
 *
 * DMU:
 * Yellow to A0
 * Red  to 5V (via filter)
 * Black to GND
 *
 * Hall circuit:
 * to A1 (as digital)
 *
 * Rear ultrasonic:
 * Vcc to 5V
 * Trig to 6
 * Echo to 7
 * Gnd to GND
 *
 * Side ultrasonic:
 * Vcc to 5V
 * Trig to 10
 * Echo to 11
 * Gnd to GND
 *
 * Left line follower:
 * Cathode to 12
 * Collector to A2
 *
 * Right line follower:
 * Cathode to 13
 * Collector to A3
 *
 * Start button:
 * to 8 and GND
 *
 * Amber LED:
 * Red 4
 * Black to GND (via 470Ohm)
 *
 * Green LED:
 * Red 3
 * Black to GND (via 470Ohm)
 *
 * Red LED:
 * Red 2
 * Black to GND (via 470Ohm)
 *
 * White LED:
 * Red to 5
 * Black to GND (via 470Ohm)
 *
 * PANIC LED: (defunct in dearth of pins)
 * Yellow to
 * Black to GND (via 470Ohm)
 */

#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

#define FIRSTSTATE MOVESTARTTO0
#define ANCHORSTATE ANGULARSCAN


/*
  Pin definitions
*/
#define LEFTMOTOR 1
#define RIGHTMOTOR 2

#define SERVO 9

#define BUTTON 8
#define DMU A0

#define HALL A1

// Ultrasonic sensors
#define FRONTTRIGGER 6
#define FRONTECHO 7
#define SIDETRIGGER 10
#define SIDEECHO 11

// Line followers
#define LEFTCATHODE 12
#define LEFTCOLLECTOR A2
#define RIGHTCATHODE 13
#define RIGHTCOLLECTOR A3

#define AMBERLED 4
#define GREENLED 3
#define REDLED 2
#define LED 5
#define PANICLED


/*
  Sensor calibration
*/
#define DMUTHRESHOLD 109
#define DMUTOLERANCE 10
#define DMURESTARTTOLERANCE 40
#define DMUFINALTHRESHOLD 380

// Note that left and right line followers are calibrated independently
#define LINELEFTTHRESHOLD 300
#define LINERIGHTTHRESHOLD 790

// Distance to stop from walls, in mm
#define WALLREARTHRESHOLD 50
#define WALLTHRESHOLD 100
#define WALLTOLERANCE 10
#define WALLFORWARDTOLERANCE 10


/*
  Sensor processing array sizes
*/
#define DMUARRAY 7
#define DMUCONFIRMARRAY 20
#define DMUCONFIRMCOUNT 12
#define DMUAPPROACHARRAY 7

#define HALLARRAY 5
#define HALLCONFIRMCOUNT 3

#define LINEFOLLOWARRAY 5


/*
  Movement speeds (out of 255)
*/
#define SCANSPEED 16
#define POSTSCANSPEED 32
#define MOVESPEED 255
#define TURNSPEED 128
#define FOLLOWSPEED 128
#define ADJUSTSPEED 100
#define APPROACHSPEED 128
#define APPROACHSCANSPEED 12
#define FINALSPEED 64


/*
  angularScan() timings, in microseconds
*/
// For scanning 90 degrees; to be divided by SCANSPEED
#define SCANDT 295000000


/*
  approach() timings, in microseconds
*/
// Timeout for when to skip to finalApproach regardless of DMU reading
#define APPROACHTIMEOUT 3600000


/*
  approachScan() timings, in microseconds
*/
// Number of sweeps, not counting initial half sweep
#define APPROACHSCANSWEEPS 4
// Time for initial half sweep
#define APPROACHSCANTI 1500000
// Time for each sweep
#define APPROACHSCANDT 3000000


/*
  finalApproach() timings, in milliseconds
*/
// Movement time for when no live mine is detected
#define FINALDT 4250
// Additional movement time after live mine is detected
#define FINALLIVEDT 2000


/*
  Turn timings, in milliseconds
  To be divided by MOVESPEED or TURNSPEED
*/
#define TURN45 114500
#define TURN90 229000
#define TURN180 458000


/*
  Movement timings, in milliseconds
  To be divided by MOVESPEED
*/
#define MOVE0TOLEFT 0L
#define MOVELEFTTOMID 460000L
#define MOVE0TOTOP 212500L
#define MOVETOPTOMID 1000000L
#define MOVEMIDTOBOTTOM 1461038L
// To prevent grabber mechanism from turning into the wall
#define WALLMANOEUVRE 250000L

// When to start going home (5.5 minutes)
#define HOMETIME 330000


/*
  retreat() and deliver() timings, in milliseconds
  To be divided by MOVESPEED
*/
#define MOVELIVE 300000L
#define MOVEDUMMYLATERAL 650000L
#define MOVEDUMMY 300000L


/*
  Grabber calibration
*/
#define SERVOANGLE 120
#define SERVODELAY 10


/*
 * Function prototypes
 */
// State transitions
void next(int);
void previous();
void anchor();
void nop();
void nopInt(int);

// States
void waitToStart();
void moveStartTo0();
void move0ToP();
void movePToP();
void angularScan();
void confirmFind();
void approach();
void approachScan();
void recover();
void retreat();
void deliverLive();
void deliverDummy();
void test();
void finalApproach();
void goHome();

// Substates
void move0To1();
void move0To2();
void move0To3();
void move0To4();
void move0To5();
void move1To2();
void move2To3();
void move3To4();
void move4To5();
void recover1();
void recover2();
void recover3();
void recover4();
void recover5();
void retreatNorth();
void retreatSouth();
void retreatWest();

// State setups
void preAngularScan();
void preConfirmFind();
void preApproach();

// General functions
void sort(int*, int, int);
long int angleRemainder();
long int angle45Remainder();

// Sensor functions
int dmu();
int rearwall();
int sidewall();
bool leftline();
bool rightline();
bool hall();

// Grabber functions
void openGrabber();
void closeGrabber();

// High level motor functions
void goLeft(int);
void goRight(int);
void goForward(int);
void goBackward(int);

// Follow functions
void goToWall();
void orthoAdjustForward();
void orthoAdjustBackward();
void goForwardToLine();
void goBackwardToLine();
void followWallBackwardToLine();
void followLineBackwardToWall();
void followWallBackwardToWall();

// Motor functions
void adjustLeft(int);
void adjustRight(int);
void forward(int);
void backward(int);
void turnRight(int);
void turnLeft(int);
void stopMotors();
// Private motor functions
void wheelForward(int);
void wheelBackward(int);

// LED functions
void blinky();
void blinkyDelay();

// PANIC
void PANIC();


/*
 * State machine pointers
 */
// Array of pointers to state-specific functions
typedef void (*FunctionPointer)();
FunctionPointer dostate[] = {
  waitToStart,
  moveStartTo0,
  move0ToP,
  movePToP,
  angularScan,
  confirmFind,
  approach,
  approachScan,
  recover,
  retreat,
  deliverLive,
  deliverDummy,
  test,
  finalApproach,
  goHome
};
FunctionPointer setupstate[] = {
  nop,  // waitToStart
  nop,  // moveStartTo0
  nop,  // move0ToP
  nop,  // movePToP
  preAngularScan,
  preConfirmFind,
  preApproach,
  nop,  // approachScan
  nop,  // recover
  nop,  // retreat
  nop,  // deliverLive
  nop,  // deliverDummy
  nop,  // test
  nop,  // finalApproach
  nop  // goHome
};
int
  WAITTOSTART = 0,
  MOVESTARTTO0 = 1,
  MOVE0TOP = 2,
  MOVEPTOP = 3,
  ANGULARSCAN = 4,
  CONFIRMFIND = 5,
  APPROACH = 6,
  APPROACHSCAN = 7,
  RECOVER = 8,
  RETREAT = 9,
  DELIVERLIVE = 10,
  DELIVERDUMMY = 11,
  TEST = 12,
  FINALAPPROACH = 13,
  GOHOME = 14
;


/*
 * Time tracking
 */
// State machine
int state = 0;
unsigned long int t0 = 0;  // Start time of current state, microseconds
int prevstate = 0;  // Previous state
unsigned long int tp = 0;  // Start time of previous state, microseconds

// Anchor state is usually ANGULARSCAN
unsigned long int tai = 0;  // Start time of anchor state, microseconds
unsigned long int taf = 0;  // End time of anchor state, microseconds

// Loop
unsigned long int tl = 0; // Start time of current loop iteration, microseconds

// Start button
unsigned long int ttt = 0; // Start time of everything; when button was pushed, milliseconds


/*
 * Motor pointers
 */
Adafruit_DCMotor *left;
Adafruit_DCMotor *right;


/*
 * DMU processing
 */
int dmuvals[DMUARRAY];  // Sliding window of DMU values
int dmuvalp = 0;  // Index of next dmuvals position
unsigned long int dmuvalt = 0;  // Running sum of dmuvals (for mean)
int dmusorted[DMUARRAY];  // Sorted dmuvals (not for persistant use outside functions; can be treated as local)

bool dmuconfirms[DMUCONFIRMARRAY];  // List of confirmation readings
int dmuconfirmp = 0;  // Index of next dmuconfirms position
int dmuconfirmc = 0;  // Running tally of confirmations (number of true values in dmuconfirms)

int dmuapproachs[DMUAPPROACHARRAY];  // Sliding window of median-dmuvals during approach
int dmuapproachp = 0; // Index of next dmuapproachs position
unsigned long int dmuapproacht = 0;  // Running sum of dmuapproachs (for mean)
int dmuapproachn = 0; // Number of values in dmuapproachs (for mean, can be less than DMUAPPROACHARRAY)

int dmuprev = 0; // Previous DMU value (for comparison over time)


/*
 * Hall effect processing
 */
int hallvals[HALLARRAY];
int hallvalp = 0;
int hallvalc = 0;


/*
 * Line follower processing
 */
int leftlines[LINEFOLLOWARRAY];
int leftlinep = 0;
int leftlinesorted[LINEFOLLOWARRAY];

int rightlines[LINEFOLLOWARRAY];
int rightlinep = 0;
int rightlinesorted[LINEFOLLOWARRAY];\


/*
 * Following functions
 */
bool leftadjusted = false;
bool rightadjusted = false;


/*
 * Mine detection
 */
bool scary = false;
bool gohome = false;

unsigned long int ledtimer;


/*
 * Scanning
 */
int corner = 1;  // Scanning position
// Array of pointers to motor-turning functions, specific to each scanning position
typedef void (*IntFunctionPointer)(int);
IntFunctionPointer scanturn[] = {nopInt, turnRight, turnLeft, turnRight, turnRight, turnRight};
IntFunctionPointer scanreverse[] = {nopInt, turnLeft, turnRight, turnLeft, turnLeft, turnLeft};
unsigned long int scantime[] = {0, SCANDT / SCANSPEED, SCANDT / SCANSPEED, SCANDT / SCANSPEED, SCANDT / SCANSPEED, SCANDT*2 / SCANSPEED};
/*
 *  1   2
 *    5
 *  3   4
 */
int approachscanit = 0;  // Number of current iterations of approachScan


/*
 * Servo
 */
Servo servo;
int serpos = 0;  // Servo position


void setup() {
  Serial.begin(9600);

  ttt = 0;

  // Setup motors
  Adafruit_MotorShield afms = Adafruit_MotorShield();
  left = afms.getMotor(LEFTMOTOR);
  right = afms.getMotor(RIGHTMOTOR);
  afms.begin();

  // Setup grabber servo
  servo.write(SERVOANGLE);
  servo.attach(SERVO);

  // Setup start button
  pinMode(BUTTON, INPUT);
  digitalWrite(BUTTON, HIGH);

  // Setup DMU
  pinMode(DMU, INPUT);

  // Setup Hall effect sensor
  pinMode(HALL, INPUT);

  // Setup ultrasonic sensors
  pinMode(FRONTTRIGGER, OUTPUT);
  pinMode(FRONTECHO, INPUT);
  pinMode(SIDETRIGGER, OUTPUT);
  pinMode(SIDEECHO, INPUT);

  // Setup line followers
  pinMode(LEFTCATHODE, OUTPUT);
  pinMode(LEFTCOLLECTOR, INPUT);
  pinMode(RIGHTCATHODE, OUTPUT);
  pinMode(RIGHTCOLLECTOR, INPUT);
  clearlines();

  // Setup LEDs
  pinMode(AMBERLED, OUTPUT);
  digitalWrite(AMBERLED, LOW);
  pinMode(GREENLED, OUTPUT);
  digitalWrite(GREENLED, LOW);
  pinMode(REDLED, OUTPUT);
  digitalWrite(REDLED, LOW);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  //pinMode(PANICLED, OUTPUT);
  //digitalWrite(PANICLED, LOW);
}


void loop() {
  blinky();
  // Only check to go home between state iterations, so as to not accidentally go home from an indeterminate position
  if (millis() > ttt + HOMETIME) {
    if (state == MOVE0TOP || state == DELIVERLIVE || state == DELIVERDUMMY) {
      next(GOHOME);
    } else {
      gohome = true;
      // Forced transition to recover(), then to goHome()
      dostate[RECOVER]();
    }
  }
  dostate[state]();
}


// Test state for running arbitrary routines. Set FIRSTSTATE to TEST to use.
void test() {

  next(WAITTOSTART);
}


void PANIC() {
  Serial.println("PANIC");
//  stopMotors();
//  digitalWrite(PANICLED, HIGH);
//  delay(1000);
//  digitalWrite(PANICLED, LOW);
}
