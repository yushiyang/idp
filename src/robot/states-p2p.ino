/*
  Move from current scanning corner (at which scanning has been completed) to the next scanning corner, then transition to angularScan()
*/
void movePToP() {
  stopMotors();
  delay(1);

  switch (corner) {
    case 1:
      move1To2();
      break;
    case 2:
      move2To3();
      break;
    case 3:
      move3To4();
      break;
    case 4:
      move4To5();
      break;
    case 5:
      // Forced transition to recover(), then to goHome()
      gohome = true;
      dostate[RECOVER]();
      break;
    default:
      PANIC();
  }

  corner++;

  next(ANGULARSCAN);
}


void move1To2() {
  goRight(TURN90);
  goToWall();
  goLeft(TURN90);
  goForward(MOVE0TOTOP);
  goRight(TURN90);
  goForward(MOVE0TOLEFT);
}


void move2To3() {
  followWallBackwardToWall();
  goRight(TURN90);
  goForward(WALLMANOEUVRE);
  goLeft(TURN180);
  followWallBackwardToLine(); // almost resets at 0
  goRight(TURN90);

  goForward(WALLMANOEUVRE);
  goLeft(TURN180);
  goToWall();
  goForward(MOVE0TOTOP);
  goRight(TURN90);
  goForward(MOVE0TOLEFT);
  goLeft(TURN90);
}


void move3To4() {
  goLeft(TURN90);
  goToWall();
  goLeft(TURN90);
  goToWall();
  goRight(TURN90);
  goForward(MOVE0TOTOP);
  goLeft(TURN90);
  goForward(MOVE0TOLEFT);
}


void move4To5() {
  goLeft(TURN90);
  goForward(WALLMANOEUVRE);
  goRight(TURN180);
  goBackwardToLine();
  goRight(TURN90);
  // already MOVE0TOTOP away from bottom
  goBackward(MOVETOPTOMID);

  goLeft(TURN90);
  goForward(MOVE0TOLEFT + MOVELEFTTOMID);
  goLeft(TURN45);
}
