void closeGrabber() {
  for (serpos = 0; serpos < SERVOANGLE; serpos++) {
    blinky();
    servo.write(serpos);
    delay(SERVODELAY);
  }
}

void openGrabber() {
  for (serpos = SERVOANGLE; serpos > 0; serpos--) {
    blinky();
    servo.write(serpos);
    delay(SERVODELAY);
  }
}
