/*
 * Left line follower:
 * Cathode to 12
 * Collector to A2
 */

#define COLLECTOR A2
#define CATHODE 12
int sensor_state;

#define LINETHRESHOLD 300

void setup() {
  Serial.begin(9600);
  pinMode(CATHODE, OUTPUT);
  pinMode(COLLECTOR, INPUT);
}

void loop() {

  digitalWrite(CATHODE, LOW);
  delayMicroseconds(2);
  digitalWrite(CATHODE, HIGH);
  delayMicroseconds(10);
  digitalWrite(CATHODE, LOW);

  sensor_state = analogRead(COLLECTOR);
  Serial.print(sensor_state);

  if (sensor_state < LINETHRESHOLD) Serial.println(" Line");
  else Serial.println("");
}
