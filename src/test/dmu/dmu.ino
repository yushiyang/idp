/*
 * DMU:
 * Yellow to A0
 * Red  to 5V (via filter)
 * Black to GND
 * 
 * Servo:
 * to Servo 2 (uses pin 9)
 * Brown on USB side
 * Orange on digital pin side
 */

#define DMU A0
#define SERVO 9
#define LED 5

#include <Servo.h>
#define SERVOANGLE 120
#define SERVODELAY 10

Servo servo;
int serpos = 0;

void openGrabber();

void setup() {
  Serial.begin(9600);
  
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  servo.write(SERVOANGLE);
  servo.attach(SERVO);
  openGrabber();
}

void loop() {
  int dmuin;

  dmuin = analogRead(DMU);

  /* 1024 at 5V */
  Serial.print(String(dmuin) + ": " + String((float) dmuin * 5 / 1024) + "V: ");
  
  if (dmuin > 512) { /* 2.5V */
    Serial.println("15-20 cm");
    digitalWrite(LED, HIGH);
  } else if (dmuin > 307) { /* 1.5V */
    Serial.println("20-40 cm or <10 cm");
    digitalWrite(LED, HIGH);
  } else if (dmuin > 205) { /* 1V */
    Serial.println("40-60 cm or <10 cm");
    digitalWrite(LED, HIGH);
  } else if (dmuin > 102) { /* 0.5 V */
    Serial.println("60-130 cm or <10 cm");
    digitalWrite(LED, HIGH);
  } else if (dmuin > 82) { /* 0.4 V */
    Serial.println("Borderline");
    digitalWrite(LED, LOW);
  } else {
    Serial.println("OoR");
    digitalWrite(LED, LOW);
  }
  
  delay(100);
}

void openGrabber() {
  for (serpos = SERVOANGLE; serpos > 0; serpos--) {
    servo.write(serpos);
    delay(SERVODELAY);
  }
}
