/*
 * Rear ultrasonic:
 * Vcc to 5V
 * Trig to 6
 * Echo to 7
 * Gnd to GND
 */

#define HALL A1
#define SERVO 9

#include <Servo.h>
#define SERVOANGLE 120
#define SERVODELAY 10

Servo servo;
int serpos = 0;

void openGrabber();

void setup() {
  Serial.begin(9600);
  pinMode(HALL, INPUT);
  
  servo.write(SERVOANGLE);
  servo.attach(SERVO);
  openGrabber();
}

void loop() {
  int hallin = digitalRead(HALL);
  if (hallin) {
    Serial.println("Hall");
  }
  
  delay(100);
}

void openGrabber() {
  for (serpos = SERVOANGLE; serpos > 0; serpos--) {
    servo.write(serpos);
    delay(SERVODELAY);
  }
}
