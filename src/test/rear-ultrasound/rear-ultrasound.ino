/*
 * Rear ultrasonic:
 * Vcc to 5V
 * Trig to 6
 * Echo to 7
 * Gnd to GND
 */

#define TRIGGER 6
#define ECHO 7

void setup() {
  Serial.begin(9600);
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
}

void loop() {
  long echoin;

  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);

  echoin = pulseIn(ECHO, HIGH);
  /* 58.1 ~= 1/0.0172; Speed of sound ~= 340 m/s */
  Serial.println(String(echoin / 58.1) + " cm");
  delay(500);
}
