/*
 * Left line follower:
 * Cathode to 13
 * Collector to A3
 */

#define COLLECTOR A3
#define CATHODE 13
int sensor_state;

#define LINETHRESHOLD 750

void setup() {
  Serial.begin(9600);
  pinMode(CATHODE, OUTPUT);
  pinMode(COLLECTOR, INPUT);
}

void loop() {

  digitalWrite(CATHODE, LOW);
  delayMicroseconds(2);
  digitalWrite(CATHODE, HIGH);
  delayMicroseconds(10);
  digitalWrite(CATHODE, LOW);

  sensor_state = analogRead(COLLECTOR);
  Serial.print(sensor_state);

  if (sensor_state < LINETHRESHOLD) Serial.println(" Line");
  else Serial.println("");
}
